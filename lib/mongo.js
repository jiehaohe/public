'use strict'

const MongoClient = require('mongodb').MongoClient;
//const config = require('../config').app.mongo;

let mongo;


module.exports = function () {
	if (!mongo) {
		let url = 'mongodb://172.17.1.111:27017/jiehao';
		mongo = MongoClient.connect(url);
	}
	return mongo;
}
