'use strict';

const request = require('request');

function Qingqiu(url, callback) {
    request.get(url, function (error, response, body) {
        if (error) {
            console.error(error);
        }

        if (response.statusCode == 200) {
            console.log("geoip body:", body);
            //res.json(body);
            return callback(null, body);
        } else {
            return callback(error, response.statusCode);
        }
    })
}

module.exports = Qingqiu;
