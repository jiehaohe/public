
module.exports = function(type){
	var assert = require('assert');
	if(type=='insert'){
		this.insertCountrys = function(db, body, callback) {
		  // Get the country collection
		  var collection = db.collection('country');
		  // Insert some country
		  collection.insertMany(
			[body]
		  , function(err, result) {
			assert.equal(err, null);
			assert.equal(1, result.result.n);
			assert.equal(1, result.ops.length);
			console.log("Inserted 1 country into the collection");
			return callback(err, result);
		  });
		}
	}else if(type=='find'){
		this.findCountrys = function(db, body, callback) {
		  // Get the country collection
		  var collection = db.collection('country');
		  // Find some country
		  collection.find(body).toArray(function(err, docs) {
			assert.equal(err, null);
			console.log("Found the following records");
			console.log('docs', docs);
			return callback(err, docs);
		  });
		}
	}else if(type=='update'){
		this.updateCountrys = function(db, bodyid, body,  callback) {
		  // Get the country collection
		  var collection = db.collection('country');
		  // Update document where a is 2, set b equal to 1
		  collection.updateOne(bodyid
			, { $set: body }, function(err, result) {
			assert.equal(err, null);
			assert.equal(1, result.result.n);
			console.log("Updated the document with the field a equal to 2");
			return callback(err, result);
		  });
		}
	}
}
