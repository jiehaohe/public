var http = require('http');
module.exports = function(ip){
	this.netip=ip;
	this.data='';
	this.find = function(req,success){
		
		var options={
			hostname: 'freegeoip.net',
			port: 80,  
			path: '/json/' + this.netip,  
			method: 'GET'  
		};
		var req = http.request(options, function(res) {
			res.setEncoding('utf8');
			res.on('data', function (data) {
			  
			  //console.log(data);
			 // res.send(data);
			 // this.data=data;
			  data = JSON.parse(data);
			  success(data);
			});
			//res.write(this.data)
		});	
		req.on('error', function(e){
		   console.log("auth_user error: " + e.message);
		});
		//req.send(this.data);
		req.end();
	}
}